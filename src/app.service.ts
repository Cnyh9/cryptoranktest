import {HttpException, Injectable} from '@nestjs/common';
import {ConvertCurrencyQueryDto} from "./request-dto/resuqest-dto";
import {HttpService} from "@nestjs/axios";
import {catchError, firstValueFrom, Observable} from "rxjs";
import {AxiosError, AxiosResponse} from "axios";
import {ConfigService} from "@nestjs/config";
import {ConfigVariables} from "./config/config.interface";

@Injectable()
export class AppService {

    constructor(
        private readonly httpService: HttpService,
        private readonly configService: ConfigService<ConfigVariables>
    ) {}

    async convert(query: ConvertCurrencyQueryDto) {
        const url = this.configService.get('apiUrl')
        const {data: {data: {price}}} = await firstValueFrom(
            this.httpService.get(url).pipe()
        )
        return this.calculate(query, price)
    }

    private calculate({from, to = 'BTC', amount = 1}: ConvertCurrencyQueryDto, price) {

        const rateTo = price[to];
        const rateFrom = price[from];

        if(!rateTo || !rateFrom) {
            throw new HttpException('Invalid currency code', 400);
            return;
        }

        // Рассчитываем конвертацию без округления
        let result = (amount * rateFrom) / rateTo;

        // Округляем результат до 15 знаков после запятой
        result = Number.parseFloat(result.toFixed(15));

        return {
            amount,
            from,
            to,
            result
        };
    }
}

import {ApiProperty} from "@nestjs/swagger";

export class ResponseDto {

    @ApiProperty({example: 'BTC'})
    from: string

    @ApiProperty({example: 'ETH'})
    to: string | null

    @ApiProperty({example: 10})
    amount: number | null

    @ApiProperty({example: 0.684720015078761})
    result: number

}

export const configuration = (): Record<string, string > => ({
    apiUrl: process.env.API_URL || 'https://tstapi.cryptorank.io/v0/coins/bitcoin'
});

type Obj = Record<string, number>
type Arr = string[]


const obj = {a: 2, b:2, c:2}
const arr = ['a', 'a', 'b', 'a/b', 'b/c', 'c']

function coinCounting(obj: Obj, arr: Arr): string[] | null {

    // Считаем общую сумму монет, доступных для розыгрыша.
    let availableCoinsSum = 0
    for (let coin of Object.values(obj)) {
        availableCoinsSum += coin
    }

    // Если количество указаных пользователем монет больше чем кол-во доступных монет - возвращаем null
    if (arr.length > availableCoinsSum) {
        console.log('NULL')
        return null
    }

    // Разделим по разным массивам одинарные монеты и смежные
    const singleCoins: string[] = []
    const doubleCoins: string[] = []
    arr.forEach((coin) => {
        !coin.includes('/') ? singleCoins.push(coin) : doubleCoins.push(coin)
    })

    const result: string[] = []

    console.log('obj', obj)
    singleCoins.forEach(coin => {
        if (!obj.hasOwnProperty(coin)) return null
        if (obj[coin] !== 0) {
            result.push(coin)
            obj[coin] = obj[coin] -1
        }
        return null
    })

    const mappedDoubleCoins = doubleCoins.map(coins => {
        const chars: string[] = coins.split('/')
        return chars
    })

    let objs = [];
    for (let i = 0; i < mappedDoubleCoins.length; i++) {
        for (let j = 0; j < arr[i].length; j++) {
            let obj:any = {};
            obj[mappedDoubleCoins[i][j]] = 1;
            obj[mappedDoubleCoins[i][(j + 1) % mappedDoubleCoins[i].length]] = 1;
            objs.push(obj);
        }
    }
    console.log('objs', objs)

    console.log('mappedDoubleCoins', mappedDoubleCoins)
    console.log('obj', obj)
    console.log('result', result)
    const filteredDoubleCoins = mappedDoubleCoins.filter(([elLeft, elRight]) => {
        if (obj[elLeft[0]] !== 0) return
    })

    return result
}

coinCounting(obj, arr)


import {IsInt, IsNumber, IsNumberString, IsOptional, IsString, Matches} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";


export class ConvertCurrencyQueryDto {

    @IsString()
    @Matches(/^[a-zA-Z]+$/,
        { message: 'FROM must contain only letters without spaces' }
    )
    @ApiProperty()
    from: string

    @IsString()
    @Matches(/^[a-zA-Z]+$/,
        { message: 'TO must contain only letters without spaces' }
    )
    @IsOptional()
    @ApiProperty({nullable: true})
    to: string | null

    @IsNumberString()
    @IsOptional()
    @ApiProperty({nullable: true})
    amount: number | null
}
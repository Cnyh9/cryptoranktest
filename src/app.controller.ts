import {Controller, Get, Query, UsePipes, ValidationPipe} from '@nestjs/common';
import { AppService } from './app.service';
import {ApiOkResponse, ApiOperation, ApiQuery, ApiTags} from "@nestjs/swagger";
import {ConvertCurrencyQueryDto} from "./request-dto/resuqest-dto";
import {ResponseDto} from "./response-dto/response-dto";

@Controller('currency')
@ApiTags('Currency')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('convert')
  @UsePipes(ValidationPipe)
  @ApiOperation({summary: 'Converting one currency to another'})
  @ApiQuery({name: 'to', required: false})
  @ApiQuery({name: 'amount', required: false})
  @ApiOkResponse({type: ResponseDto})
  async convert(@Query() query: ConvertCurrencyQueryDto) {
    return await this.appService.convert(query)
  }
}
